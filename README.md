# README #

## What's PasswordGame ##
This is a programming assignment from Artificial Intelligence course from University of Tartu. 

Idea is to create a program (AI) that can guess the password the user has though or it's vise versa (user guesses the password AI has thought). 
In order to succeed feedback is given or asked after each round.

Program is written in Java as a command line application. 

![2015-10-23 10.54.37 pm.png](https://bitbucket.org/repo/aaBLBo/images/17273167-2015-10-23%2010.54.37%20pm.png)

Full assignment test (in Estonian):
*Koostada programm, mis mängib mängu "Salasõna".

2 mängijat, üks neist võib olla arvuti, rollid võiks olla vahetatavad. Kombinatsiooni pikkus võiks olla etteantav parameeter.

1. mängija koostab 7-kohalise kombinatsiooni (loe: permutatsiooni) 7 värvist, värvid võivad korduda (2. mängijale nähtamatult). 2. mängija peab salasõna ära arvama: pakub 7-kohalise kombinatsiooni. 2. mängija vastab, mitmel positsioonil oli õige värv, ütlemata, millised konkreetselt need positsioonid on, ja lisaks, mitu oli veel õiget värvi, kuid valel positsioonil. Mängijad jätkavad pakkumist ja vastamist kuni õige lahenduseni.*

## How to install and run ##
1. Navigate to folder PasswordGame
2. Compile the program: 
```
#!

javac -d bin -sourcepath src src/com/ai/hw/PasswordGame.java
```

3. Run the program: 
```
#!

java com.ai.hw.PasswordGame
```