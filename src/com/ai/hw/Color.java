package com.ai.hw;

public class Color {

	public final int num;
	public final char code;
	public final String desc;

	public Color(int num, String desc) {
		this.num = num;
		this.code = desc.charAt(0);
		this.desc = desc;		
	}

}
