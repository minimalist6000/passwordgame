package com.ai.hw;

import java.util.Random;

public class Knower {

	private int[] password;

	public void createPassword(){
		Random rn = new Random();

		this.password = new int[PasswordGame.passwordLenght];
	    for (int i = 0; i < PasswordGame.passwordLenght; i++) {
	    	this.password[i] = rn.nextInt(PasswordGame.passwordMaxItemValue);
		
		}
		
	}

	public Answer checkPassword(String guesserPassword) {
		int[] gp = new int[guesserPassword.length()];
	    
		for(int i = 0, n = guesserPassword.length() ; i < n ; i++) { 
			if (PasswordGame.passwordType == 'C'){
				Color color = PasswordGame.colors.get(guesserPassword.charAt(i));
				gp[i] = color.num; 
			}else{
				gp[i] = guesserPassword.charAt(i) - '0';
			}
		}
		
		return checkPassword(gp);
	}

	public Answer checkPassword(int[] guesserPassword){
		int correctItemsInCorrectPossitions = getCorrectItemsInCorrectPositions(guesserPassword);
		int correctItemsInFalsePossitions = getCorrectItemsInFalsePositions(guesserPassword);

		return new Answer(correctItemsInCorrectPossitions, correctItemsInFalsePossitions);
	}


	private int getCorrectItemsInCorrectPositions(int[] guesserPassword) {
		int foundCnt = 0;
		for(int i = 0, n = guesserPassword.length ; i < n ; i++) { 
			if (this.password[i] == guesserPassword[i]){
				foundCnt++;
			}	 
		}		
		return foundCnt;
	}
	
	private int getCorrectItemsInFalsePositions(int[] guesserPassword) {
		int foundCnt = 0;
		for(int i = 0, n = guesserPassword.length ; i < n ; i++) { 
			if (this.password[i] != guesserPassword[i] && isIntInPassword(i)){
				foundCnt++;
			}	 
		}		
		return foundCnt;
	}


	private boolean isIntInPassword(int i) {
		for (int j : this.password) {
			if (i == j){
				return true;
			}
		}
		return false;
	}


	public int[] getPassword() {
		return password;
	}


	public void setPassword(int[] password) {
		this.password = password;
	}

}
