package com.ai.hw;

import java.util.Scanner;

public class KnowerGame {

	private Scanner sc;
	private int[] password;
	private boolean isAborted;
	private boolean isCorrect;
	private int baseCnt;
	
	public KnowerGame(){
		this.sc = new Scanner(System.in);
		this.isAborted = false;
		this.isCorrect = false;
	}
	
	public void play() {
		showInstructions();
		if (doesUserWantToPlay()){
			guessPassword();
			if (this.isCorrect){		
				System.out.println("Your password was: '" + getPasswordAsString(this.password) + "'.");	
			}
		} else {
			this.isAborted = true;
		}
		
		if (this.isAborted){
			System.out.println("Game aborted.");				
		}
	}

	private void showInstructions() {
		String pwTypeStr = PasswordGame.passwordType == 'C' ? "colors" : "numbers";
		System.out.println("Please think of a password that is composed of " + PasswordGame.passwordLenght + " " + pwTypeStr + ".");
		System.out.println("It should contain following " + pwTypeStr + ": ");
		PasswordGame.showComponents();
		System.out.println("When you're ready type 'GO' and AI tries to guess it.");	
		System.out.println("Type 'QUIT' to end the game.");		
	}	

	private boolean doesUserWantToPlay() {
		String reply = null;
		try {
		  reply = this.sc.next().toUpperCase();		
		} catch (Exception e) {}
		return !reply.equals("QUIT");
	}

	private void guessPassword(){
		int currentElement = 0;
		this.password = createInitialPassword();

		this.baseCnt = checkPassword(this.password);
		if (this.isCorrect || this.isAborted) return;
	
		do {			
			guessCurrentElement(currentElement);
			currentElement++;
		} while (currentElement < PasswordGame.passwordLenght && this.isAborted == false && this.isCorrect == false);
	}
	
	
	private int[] createInitialPassword() {
		int[] pw = new int[PasswordGame.passwordLenght];
		int initialElement = 0;
		for (int i = 0; i < PasswordGame.passwordLenght; i++) {		
			pw[i] = initialElement;
		}
		return pw;
	}
	

	private void guessCurrentElement(int currentElement) {
		int[] pw = this.password.clone();
		boolean foundCurrentElement = false;
		do {
			// mod current element (by inc. by one)
			pw = incCurrentElement(currentElement, pw);
			if (this.isAborted) return;
			
			int modCnt = checkPassword(pw);
			if (this.isAborted) return;
	
			if (this.isCorrect || this.baseCnt < modCnt){
				// Mod was correct. Update the password.
				foundCurrentElement = true;
				this.password = pw;
				this.baseCnt = modCnt;
			}
			else if (this.baseCnt > modCnt){
				// Base was correct. Don't modify password.
				foundCurrentElement = true;
			}
						
		} while (!foundCurrentElement);
	}

	private int[] incCurrentElement(int currentElement, int[] pw) {
		pw[currentElement]++;
		if (pw[currentElement] == PasswordGame.passwordMaxItemValue){
			System.out.println("Error - all possible combinations for element #" + (currentElement + 1) + " where tried.");
			this.isAborted = true;
		}
		return pw;
	}
	
	private int checkPassword(int[] pw) {
		String pwStr = getPasswordAsString(pw);
		String pwTypeStr = PasswordGame.passwordType == 'C' ? "colors" : "numbers";

		System.out.println("Is the password: '" + pwStr + "' ?");
		System.out.println("Type 'Y' when it's correct.");
	    System.out.println("Or enter how many " + pwTypeStr + " are correct in correct position? (0 - " + PasswordGame.passwordLenght + ")");
	
    	String reply = getReplyAsString();
		if (reply.equals("QUIT")){
			this.isAborted = true;
			return 0;
		}

		if (reply.equals("Y")){
			this.isCorrect = true;
			return PasswordGame.passwordLenght; 
		} else{
			int correctItemsInCorrectPossitions = Integer.parseInt(reply);
			this.isCorrect = correctItemsInCorrectPossitions == PasswordGame.passwordLenght;
			return correctItemsInCorrectPossitions;
 		}
	}

	private String getPasswordAsString(int[] pw) {
		StringBuilder sb = new StringBuilder();

		for (int i : pw) {
			if (PasswordGame.passwordType == 'C'){
				Color c = PasswordGame.colors.get(PasswordGame.colorNums[i]);
				sb.append(c.desc + " ");				
			}else{
				sb.append(i);				
			}
		}
		
		return sb.toString().trim();
	}

	private String getReplyAsString() {
		String s = null;
		boolean isValidReply = false;
		do{
			try {
				s = this.sc.next().toUpperCase();				
			} catch (Exception e) {
				this.sc.next();
			}
			isValidReply = isValidReply(s);
			if (!isValidReply){
				System.out.println("This is invalid reply. Use 'Y' or 'QUIT' or '0' ... '" + PasswordGame.passwordLenght + "'");				
			}
		} while (!isValidReply);		
		
		return s;
	}
	

	private boolean isValidReply(String s) {
		if (s.equals("Y")) return true;
		if (s.equals("QUIT")) return true;
		try {
			int i = Integer.parseInt(s);
			if (i >= 0 && i <= PasswordGame.passwordLenght) return true;
		} catch (Exception e) {}
		return false;
	}
}
