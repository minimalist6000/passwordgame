package com.ai.hw;

import java.util.HashMap;
import java.util.Scanner;

public class PasswordGame {

	public static int passwordLenght;
	public static int passwordMaxItemValue;
	public static char passwordType;
	public static HashMap<Character, Color> colors;
	public static char[] colorNums;
	private char playerType;
	private Scanner sc;

	public PasswordGame(){	
		this.sc = new Scanner(System.in);
		passwordType = 'C';	
		passwordLenght = 7;
		passwordMaxItemValue = 7;
		setColors();
	}	
	
	public static void main(String[] args) {
		PasswordGame game = new PasswordGame();
		game.initGame();
        //game.initGameWithDebugParams();

		if (game.playerType == 'G'){
			GuesserGame gg = new GuesserGame();
			gg.play();
		}
		else{
			KnowerGame kg = new KnowerGame();
			kg.play();
		}
	}

	public static void showComponents() {
		if (PasswordGame.passwordType == 'C'){
			for (int i = 0; i < PasswordGame.passwordMaxItemValue; i++) {
				Color color = PasswordGame.colors.get(PasswordGame.colorNums[i]);
				System.out.println(color.code + " - " + color.desc);
			}
		}
		else{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < PasswordGame.passwordMaxItemValue; i++) {
				sb.append(i == 0 ? i : ", " + i);
									
			}
			System.out.println(sb.toString());
		}
	}

	private void setColors() {
		colors = new HashMap<Character, Color>();
		colors.put('R', new Color(0, "Red"));
		colors.put('G', new Color(1, "Green"));
		colors.put('B', new Color(2, "Blue"));
		colors.put('W', new Color(3, "White"));
		colors.put('Y', new Color(4, "Yellow"));
		colors.put('O', new Color(5, "Orange"));
		colors.put('P', new Color(6, "Purple"));
		colors.put('S', new Color(7, "Silver"));
		colors.put('L', new Color(8, "Lime"));
		
		colorNums = new char[]{'R','G','B','W','Y','O','P','S','L'};
	}

	private void initGame() {
		System.out.println("Password Game by Indrek Ilves");
		System.out.println("------------------------------");
		System.out.println("Guess the password created by AI or let AI guess your password.");
		System.out.println("Password contains of colors or numbers.");
		System.out.println("Default values are:");
		System.out.println("  Password type: C - Colors");
		System.out.println("  Password lenght: 7");
		System.out.println("  Max # of different colors / numbers: 7");
		System.out.println("------------------------------");
		if (!useDefaults()){
			passwordType = getPasswordType();
			passwordLenght = getPasswordLenght();
			passwordMaxItemValue = getPasswordMaxItemValue();
		}
		this.playerType = getPlayerType();
		
	}

	private boolean useDefaults() {
		boolean ud = false;
		System.out.println("Use defaults? (Y/N):");
		try {
			ud = this.sc.next().toUpperCase().charAt(0) == 'Y';				
		} catch (Exception e) {
			this.sc.next();
			ud = false;
		}
		return ud;
	}
	
	private char getPlayerType() {
		char pt = '-';
		do{
			System.out.println("Do you want to be guess AI password or let AI guess your password?");
			System.out.println("(Enter 'G' for password guesser or 'K' for password knower):");
			try {
				pt = this.sc.next().toUpperCase().charAt(0);				
			} catch (Exception e) {
				this.sc.next();
			}
			if (pt != 'G' && pt != 'K'){
				System.out.println("This is invalid type.");				
			}
		} while (pt != 'G' && pt != 'K');		
		
		return pt;
	}


	private char getPasswordType() {
		char pwT = '-';
		do{
			System.out.println("Use colors or numbers? (Enter 'C' for Colors or 'N' for numbers):");
			try {
				pwT = this.sc.next().toUpperCase().charAt(0);				
			} catch (Exception e) {
				this.sc.next();
			}
			if (pwT != 'C' && pwT != 'N'){
				System.out.println("This is invalid type.");				
			}
		} while (pwT != 'C' && pwT != 'N');		
		
		return pwT;
	}

	private int getPasswordLenght() {
		int pwL = 0;
		do{
			System.out.println("How long the password should be? (1 - 10):");
			try {
				pwL = this.sc.nextInt();				
			} catch (Exception e) {
				this.sc.next();
			}
			if (pwL < 1 || pwL > 10){
				System.out.println("This is invalid lenght.");				
			}
		} while (pwL < 1 || pwL > 10);
		return pwL;
	}
		
	private int getPasswordMaxItemValue() {
		int pwMiv = 0;
		do{
			System.out.println("How many colors / numbers can be used? (1-9):");
			try {
				pwMiv = this.sc.nextInt();				
			} catch (Exception e) {
				this.sc.next();
			}
			if (pwMiv < 1 || pwMiv > 9){
				System.out.println("This is invalid number.");				
			}
		} while (pwMiv < 1 || pwMiv > 9);
		return pwMiv;
	}


	@SuppressWarnings("unused")
	private void initGameWithDebugParams() {
		//Debug params
		passwordType = 'C';	
		passwordLenght = 3;
		passwordMaxItemValue = 3;
		this.playerType = 'K';
	}

}
