package com.ai.hw;

import java.util.Scanner;

public class GuesserGame {

	private Knower knower;
	
	public GuesserGame(){
		this.knower = new Knower();
	}
	
	
	public void play() {
		this.knower.createPassword();
		//this.knower.setPassword(new int[]{0,1,2});
		showInstructions();
		boolean isCorrect = false;
		String guess;
		String errMsg;
		do{
			System.out.println("Enter your guess: (type: 'Quit' to end the game).");
			
			guess = getGuess();
			if (guess.equals("QUIT")){
				System.out.println("Game aborted.");				
				return;
			}
			
			errMsg = isValidGuess(guess); 
			if (errMsg.isEmpty()){
				isCorrect = checkGuess(guess);
			} else {
				System.out.println("Invalid guess. " + errMsg);				
			}

		} while (isCorrect == false);
		System.out.println("You win. " + guess + " is the correct password.");					
	}

	private void showInstructions() {
		String pwTypeStr = PasswordGame.passwordType == 'C' ? "colors" : "numbers";
		System.out.println("AI has thought of a password composed of " + PasswordGame.passwordLenght + " " + pwTypeStr + ".");
		System.out.println("It contains following " + pwTypeStr + ": ");
		PasswordGame.showComponents();
		System.out.println("Try to guess it. After you try AI will tell you:");	
		System.out.println("1. How many " + pwTypeStr + " are correct in correct position.");
		System.out.println("2. How many " + pwTypeStr + " are correct but in false position.");	
		System.out.println("OK here we go....");	
	}


	private String getGuess() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		String guessStr = "";
		try {
			guessStr = sc.next().toUpperCase();
		} catch (Exception e) {
			sc.next();
		}	
		return guessStr;
	}

	private String isValidGuess(String guessStr) {
		if (guessStr.length() != PasswordGame.passwordLenght){
			return "Guess length is incorrect.";
		}
				
		if (PasswordGame.passwordType == 'C' && containsInvalidColor(guessStr)){
			return "Guess contains invalid color.";
		}

		if (PasswordGame.passwordType == 'N' && containsInvalidNumber(guessStr)){
			return "Guess contains invalid number.";
		}

		return "";
	}

	private boolean containsInvalidColor(String guessStr) {
		for (char c : guessStr.toCharArray()) {
			if (!PasswordGame.colors.containsKey(c)){
				return true;
			}			
		}		
		return false;
	}
		
	private boolean containsInvalidNumber(String guessStr) {
		int i;
		for (char c : guessStr.toCharArray()) {
			try {
				i = Character.getNumericValue(c);			
			} catch (Exception e) {
				return true;
			}
			if (i < 0 || i > PasswordGame.passwordMaxItemValue){
				return true;
			}			
		}		
		return false;
	}

	private boolean checkGuess(String guess) {
		Answer a = this.knower.checkPassword(guess);
		if (!a.isCorrect){
			String pwTypeStr = PasswordGame.passwordType == 'C' ? "color(s)" : "number(s)";
			System.out.println(a.correctItemsInCorrectPossitions + " correct " + pwTypeStr + " in correct position.");	
			System.out.println(a.correctItemsInFalsePossitions + " correct " + pwTypeStr + " in false position.");	
		}
		return a.isCorrect;
	}
}
