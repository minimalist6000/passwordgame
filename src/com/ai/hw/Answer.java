package com.ai.hw;

public class Answer {


	public final int correctItemsInCorrectPossitions;
	public final int correctItemsInFalsePossitions;
	public final boolean isCorrect;

	public Answer(boolean isCorrect) {
		this.correctItemsInCorrectPossitions = isCorrect ? PasswordGame.passwordLenght : 0;
		this.correctItemsInFalsePossitions = 0;
		this.isCorrect = isCorrect;
	}
	
	public Answer(int correctItemsInCorrectPossitions,int correctItemsInFalsePossitions) {
		this.correctItemsInCorrectPossitions = correctItemsInCorrectPossitions;
		this.correctItemsInFalsePossitions = correctItemsInFalsePossitions;
		this.isCorrect = correctItemsInCorrectPossitions == PasswordGame.passwordLenght;
	}

	
}
